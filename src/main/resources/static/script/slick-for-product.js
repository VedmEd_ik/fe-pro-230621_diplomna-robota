// Слайдер відкритого товару - зміна стандартних параметрів
$('.product__slider').slick({
	slidesToShow: 1,
	slidesToScroll: 1,
	arrows: true,
	fade: true,
	asNavFor: '.product__slider-nav'
});
$('.product__slider-nav').slick({
	slidesToShow: 3,
	slidesToScroll: 1,
	asNavFor: '.product__slider',
	dots: false,
	centerMode: true,
	focusOnSelect: true,
	arrows: false,
});