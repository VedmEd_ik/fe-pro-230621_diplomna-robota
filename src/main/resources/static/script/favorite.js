'use strict';

// Функція перевірки чи є вже даний товар в "Вибране"
function productInFavorite(productId) {
	const favorite = document.querySelector('.favorite__products-in-favorite');
	// Отримуємо в масив усіх ід товарів, які є в "Вибране"
	const allProductsId = [].map.call(favorite.querySelectorAll('.favorite__product-item[data-prodid]'), function (el) {
		return el.dataset.prodid;
	});

	if (allProductsId.includes(productId)) {
		removeProductInFavorite(productId);
	} else {
		addToFavorite(productId);
	}
};

// Функція додавання товару У "Вибране"
function addToFavorite(productId) {
	// Отримую вихідні дані
	const product = document.querySelector(`[data-prodid='${productId}']`);
	const productImage = product.querySelector('.product-card__img img');
	const productTitle = product.querySelector('.product-card__title').textContent;

	const favoriteTotal = document.querySelector('.favorite__total');

	const productItem = document.createElement('div');
	productItem.classList.add('favorite__product-item');
	productItem.dataset.prodid = productId;
	favoriteTotal.before(productItem);

	const wrapImgProduct = document.createElement('div');
	wrapImgProduct.classList.add('favorite__img-product');
	productItem.append(wrapImgProduct);

	const imgProduct = document.createElement('img');
	imgProduct.setAttribute('src', productImage.src);
	imgProduct.setAttribute('alt', 'image');
	wrapImgProduct.append(imgProduct);

	const urlProduct = document.createElement('a');
	urlProduct.setAttribute('href', '#');
	urlProduct.textContent = productTitle;
	wrapImgProduct.append(urlProduct);

	const close = document.createElement('div');
	close.classList.add('close');
	productItem.append(close);

	const imgClose = document.createElement('img');
	imgClose.setAttribute('src', "./image/icon/close.svg");
	close.append(imgClose);

	updateIconFavorite();
}

// Функція оновлення лічильника доданих товарів у "Вибране" біля іконки "Вибране"
function updateIconFavorite() {
	const favorite = document.querySelector('.header__favorite');
	const favoriteIcon = favorite.querySelector('.favorite__icon-link');
	const favoriteSpan = favoriteIcon.querySelector('span');
	const favoriteQuantity = totalCountProductsInFavorite();

	if (favoriteQuantity === 0) {
		if (favoriteSpan) {
			favoriteSpan.remove();
		}
	} else {
		if (favoriteSpan) {
			favoriteSpan.innerHTML = totalCountProductsInFavorite();
		} else {
			favoriteIcon.insertAdjacentHTML('beforeend', `<span>${totalCountProductsInFavorite()}</span>`)
		};
	}
}

// Функція підрахунку загальної кількості товарів у "Вибране"
function totalCountProductsInFavorite() {
	const favoriteTotalInfo = document.querySelector('.favorite__total');
	const allProductsInFavorite = document.querySelectorAll('.favorite__product-item');
	favoriteTotalInfo.textContent = allProductsInFavorite.length;

	return allProductsInFavorite.length;
};

// Функція зміни іконки "Вибране" на картці товару
function changeIconLike(productId) {
	const cardsWrapper = document.querySelector('.cards-wrapper');
	const product = cardsWrapper.querySelector(`[data-prodid='${productId}']`);
	const likeImg = product.querySelector('.product-card__favorite img');

	if (product.dataset.favor === "false") {
		likeImg.setAttribute('src', './image/icon/favorite-fiolet.svg');
		product.dataset.favor = true;
	} else if (product.dataset.favor === "true") {
		likeImg.setAttribute('src', './image/icon/heart-like2.svg');
		product.dataset.favor = false;
	}
}

// Функція видалення товару з "Вибраного"
function removeProductInFavorite(productId) {
	const favorite = document.querySelector('.favorite__products-in-favorite');
	favorite.querySelector(`[data-prodid='${productId}']`).remove();
	updateIconFavorite();
}