'use strict';

// Фільтрування товарів
function filterProducts(targetElement) {

	// Отримуємо значення дата-атрибуту елемента nav-menu__item, по якому відбувся клік
	let navMenuItem = targetElement.closest('.nav-menu__item');
	let category = navMenuItem.dataset.filter;

	// Отримуємо дата-атрибути усіх товарів
	let allProducts = document.querySelectorAll('[data-category]');

	// Якщо отримане значення дата-атрибуту == 'all', то показуємо усі іграшки
	if (category == 'all') {
		allProducts.forEach(elem => {
			elem.classList.remove('hide');
		})
	} else {
		// Перебираємо усі елементи з дата-атрибутом "data-category" і фільтруємо
		allProducts.forEach(item => {
			let workCat = item.dataset.category;

			if (workCat !== category) {
				item.classList.add('hide')
			} else {
				item.classList.remove('hide');
			};
		})
	};
}

// Функція для отримання id останньої іграшки на сайті і створення запиту на сервер для отримання товарів
function getProducts(button) {
	const lastProduct = document.querySelector('.cards-wrapper').lastElementChild;

	if (lastProduct) {
		const idLastProduct = lastProduct.dataset.prodid;
		const getProducts = new GetProductsFromServer(button, idLastProduct);
		getProducts.getProductsFromServer();

	} else if (!lastProduct) {
		const getProducts = new GetProductsFromServer(button, 0);
		getProducts.getProductsFromServer();
	}
}

// Завантаження додаткових товарів
function loadProducts(data) {
	const cardsWrapper = document.querySelector('.cards-wrapper');

	data.forEach(element => {
		// Контейнер - картка іграшки
		const productCard = document.createElement('div');
		productCard.classList.add('product-card');
		productCard.dataset.category = element.type;
		productCard.dataset.prodid = element.id;
		productCard.dataset.favor = false;

		// Іконка "Вибране"
		const productCardFavorite = document.createElement('div');
		productCardFavorite.classList.add('product-card__favorite');
		productCard.append(productCardFavorite);
		const iconFavorite = document.createElement('img');
		iconFavorite.src = './image/icon/heart-like2.svg';
		iconFavorite.alt = "Favorite";
		productCardFavorite.append(iconFavorite);

		// Labels - значки "Скидка" і "New"
		if (element.labels) {
			const productCardLabels = document.createElement('div');
			productCardLabels.classList.add('product-card__labels');

			element.labels.forEach((label) => {
				if (label === 'SALE') {
					const productCardLabelDiscount = document.createElement('div');
					productCardLabelDiscount.classList.add('product-card__label-discount');
					productCardLabelDiscount.classList.add('label');
					productCardLabelDiscount.textContent = `-${element.price.discount}%`;
					productCardLabels.append(productCardLabelDiscount);
				} else if (label === 'NEW') {
					const productCardLabelNew = document.createElement('div');
					productCardLabelNew.classList.add('product-card__label-new');
					productCardLabelNew.classList.add('label');
					productCardLabelNew.textContent = label;
					productCardLabels.append(productCardLabelNew);
				};
			});
			productCard.append(productCardLabels);
		};

		// Контейнер для картинки
		const productCardImg = document.createElement('div');
		productCardImg.classList.add('product-card__img');
		productCard.append(productCardImg);

		// Картинка
		const img = document.createElement('img');
		img.src = element.mainImage.source;
		img.alt = element.type;
		productCardImg.append(img);

		// Заголовок - імя іграшки
		const productCardTitle = document.createElement('div');
		productCardTitle.classList.add('product-card__title');
		productCardTitle.textContent = element.title;
		productCard.append(productCardTitle);

		// Підзаголовок - опис іграшки
		const productCardSubtitle = document.createElement('div');
		productCardSubtitle.classList.add('product-card__subtitle', 'description');
		productCardSubtitle.textContent = element.description;
		productCard.append(productCardSubtitle);

		// Підзаголовок - ріст іграшки
		const productCardSubtitleHeight = document.createElement('div');
		productCardSubtitleHeight.classList.add('product-card__subtitle', 'height');
		productCardSubtitleHeight.textContent = `Ріст ${element.height} см`;
		productCard.append(productCardSubtitleHeight);

		// Контейнер для ціни іграшки
		const productCardPrice = document.createElement('div');
		productCardPrice.classList.add('product-card__price');
		productCard.append(productCardPrice);

		// Ціна іграшки без знижки
		const productCardPriceWithoutDiscount = document.createElement('div');
		productCardPriceWithoutDiscount.classList.add('product-card__price-without-discount');
		productCardPriceWithoutDiscount.textContent = `${element.price.initialPrice} грн.`;
		productCardPrice.append(productCardPriceWithoutDiscount);

		// Ціна іграшки зі знижкою
		if (element.price.priceWithDiscount) {
			const productCardPriceWidhDiscount = document.createElement('div');
			productCardPriceWidhDiscount.classList.add('product-card__price-widh-discount');
			productCardPriceWidhDiscount.textContent = `${element.price.priceWithDiscount} грн.`;
			productCardPrice.append(productCardPriceWidhDiscount);
			productCardPriceWithoutDiscount.style.textDecoration = 'line-through';
		}

		// Кнопка "Купити"
		const productCardButton = document.createElement('button');
		productCardButton.classList.add('product-card__button');
		productCardButton.textContent = "Купити";
		productCard.append(productCardButton);

		// Добавити карту продукту на сторінку
		cardsWrapper.append(productCard);
	});
}

// Функція, яка показує фото та опис товару при кліці на нього
function showProduct(urlPhoto, pId) {
	const mainProduct = document.querySelector(`[data-prodid="${pId}"]`);
	const title = mainProduct.querySelector('.product-card__title').textContent;
	const description = mainProduct.querySelector('.description').textContent;
	const height = mainProduct.querySelector('.height').textContent;

	// Обгортка для товару
	const productWrapper = document.createElement('div');
	productWrapper.classList.add('product-wrapper');
	document.body.append(productWrapper);

	// Товар
	const product = document.createElement('div');
	product.classList.add('product');
	product.dataset.prodid = pId;
	productWrapper.append(product);

	// Обгортка для слайдера
	const productSliderWrapper = document.createElement('div');
	productSliderWrapper.classList.add('product__slider-wrapper');
	product.append(productSliderWrapper);

	// Основний слайдер
	const productSlider = document.createElement('div');
	productSlider.classList.add('product__slider');
	productSliderWrapper.append(productSlider);

	// Нижній слайдер
	const productSliderNav = document.createElement('div');
	productSliderNav.classList.add('product__slider-nav');
	productSliderWrapper.append(productSliderNav);

	// Елементи слайдерів
	for (let photo of urlPhoto) {

		// Слайд для основного слайдера
		const productSliderItem = document.createElement('div');
		productSliderItem.classList.add('product__slider-item');
		productSlider.append(productSliderItem);

		// Картинка для слайду основного слайдера
		const productSliderImg = document.createElement('img');
		productSliderImg.setAttribute('src', photo.source);
		productSliderImg.classList.add('product__slider-img');
		productSliderItem.append(productSliderImg);

		// Слайд для нижнього слайдера
		const productSliderNavItem = document.createElement('div');
		productSliderNavItem.classList.add('product__slider-nav-item');
		productSliderNav.append(productSliderNavItem);

		// Картинка для слайду нижнього слайдера
		const productSliderNavImg = document.createElement('img');
		productSliderNavImg.setAttribute('src', photo.source);
		productSliderNavImg.classList.add('product__slider-nav-img');
		productSliderNavItem.append(productSliderNavImg);
	};

	// Обгортка для опису товару
	const productDescriptionWrapper = document.createElement('div');
	productDescriptionWrapper.classList.add('product__description-wrapper');
	product.append(productDescriptionWrapper);

	// Опис товару
	const productDescription = document.createElement('div');
	productDescription.classList.add('product__description');
	productDescriptionWrapper.append(productDescription);

	// Назва товару
	const productTitle = document.createElement('div');
	productTitle.classList.add('product__title');
	productTitle.textContent = title;
	productDescription.append(productTitle);

	// Підзаголовок - опис іграшки
	const productCardSubtitle = document.createElement('div');
	productCardSubtitle.classList.add('product__text');
	productCardSubtitle.textContent = description;
	productDescription.append(productCardSubtitle);

	// Підзаголовок - ріст іграшки
	const productCardSubtitleHeight = document.createElement('div');
	productCardSubtitleHeight.classList.add('product__text');
	productCardSubtitleHeight.textContent = height;
	productDescription.append(productCardSubtitleHeight);

	// Ціна товару
	const productPrice = document.createElement('div');
	productPrice.classList.add('product__price');


	if (mainProduct.querySelector('.product-card__price-widh-discount')) {
		const priceWidhDiscount = mainProduct.querySelector('.product-card__price-widh-discount').textContent;
		productPrice.textContent = priceWidhDiscount;
	} else {
		const priceWidhoutDiscount = mainProduct.querySelector('.product-card__price-without-discount').textContent;
		productPrice.textContent = priceWidhoutDiscount;
	}

	productDescription.append(productPrice);

	// Кнопка "Купити"
	const productButton = document.createElement('button');
	productButton.classList.add('product__button');
	productButton.textContent = 'Купити';
	productDescription.append(productButton);

	// Кнопка "Закрити"
	const close = document.createElement('div');
	close.classList.add('close');
	product.append(close);
	const imgClose = document.createElement('img');
	imgClose.setAttribute('src', './image/icon/close.svg');
	close.append(imgClose);

	// Запускаємо слайдер для товару
	$('.product__slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		fade: true,
		asNavFor: '.product__slider-nav'
	});
	$('.product__slider-nav').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		asNavFor: '.product__slider',
		dots: false,
		centerMode: true,
		focusOnSelect: true,
		arrows: false,
	});
}