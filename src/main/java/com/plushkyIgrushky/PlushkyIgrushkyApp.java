package com.plushkyIgrushky;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlushkyIgrushkyApp {

    public static void main(String[] args) {
        SpringApplication.run(PlushkyIgrushkyApp.class, args);
    }
}
