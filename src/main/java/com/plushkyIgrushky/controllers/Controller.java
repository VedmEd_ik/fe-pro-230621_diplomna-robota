package com.plushkyIgrushky.controllers;

import com.plushkyIgrushky.dto.product.ImageDTO;
import com.plushkyIgrushky.dto.product.PriceDTO;
import com.plushkyIgrushky.dto.product.PublishedToyDTO;
import com.plushkyIgrushky.dto.product.toy.ToyDTO;
import com.plushkyIgrushky.dto.results.ResultDTO;
import com.plushkyIgrushky.dto.results.SuccessResult;
import com.plushkyIgrushky.service.GeneralService;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Controller {

    private static final int PAGE_SIZE = 8;

    private GeneralService generalService;

    public Controller(GeneralService generalService) {
        this.generalService = generalService;
    }

    @PostMapping("toys")
    public List<ToyDTO> getToys(@RequestBody Integer counter){
        System.out.println(generalService.getToys(PageRequest.of(
                counter,
                PAGE_SIZE,
                Sort.Direction.DESC,
                "id"
        )));
        return generalService.getToys(PageRequest.of(
                counter,
                PAGE_SIZE,
                Sort.Direction.DESC,
                "id"
        ));
    }

    @GetMapping("/images")
    public List<ImageDTO> getImages(Long idProduct){
        return generalService.findImagesByProduct(idProduct);
    }

    @PostMapping("/addtoy")
    public ResponseEntity<ResultDTO> addToy(@RequestBody PublishedToyDTO publishedToyDTO){
        System.out.println(publishedToyDTO);
        ToyDTO toy = publishedToyDTO.getToyDTO();
        PriceDTO price = publishedToyDTO.getPriceDTO();
        ImageDTO mainImage = publishedToyDTO.getMainImage();
        List<ImageDTO> images = publishedToyDTO.getImageDTO();

        generalService.addToy(toy, price, mainImage, images);

        return new ResponseEntity<>(new SuccessResult(), HttpStatus.OK);
    }
}
