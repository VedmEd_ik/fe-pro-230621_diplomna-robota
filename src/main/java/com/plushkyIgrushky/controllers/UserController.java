package com.plushkyIgrushky.controllers;

import com.plushkyIgrushky.dto.results.ResultDTO;
import com.plushkyIgrushky.dto.results.SuccessResult;
import com.plushkyIgrushky.dto.results.UserExists;
import com.plushkyIgrushky.dto.user.CustomUserDTO;
import com.plushkyIgrushky.model.user.CustomUser;
import com.plushkyIgrushky.model.user.UserRole;
import com.plushkyIgrushky.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    private final PasswordEncoder passwordEncoder;
    private final UserService userService;

    public UserController(PasswordEncoder passwordEncoder, UserService userService) {
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
    }

    @PostMapping(value = "/newuser")
    public  ResponseEntity<ResultDTO> update(@RequestBody CustomUserDTO userDTO){

        String passHash = passwordEncoder.encode(userDTO.getPassword());

        CustomUser user = new CustomUser(userDTO.getFirstName(), userDTO.getEmail(), passHash, UserRole.USER);

        if (!userService.addUser(user.toDTO())) {
            return new ResponseEntity<>(new UserExists(), HttpStatus.OK);
        }

        return new ResponseEntity<>(new SuccessResult(), HttpStatus.OK);
    }

    @PostMapping(value = "/loginuser")
    public ResponseEntity<ResultDTO> login(@RequestBody CustomUserDTO userDTO){
        return userService.login(userDTO.getEmail(), userDTO.getPassword());
    }
}
