package com.plushkyIgrushky.model.product;

import com.plushkyIgrushky.dto.product.PriceDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "prices")
@NoArgsConstructor
@Getter
@Setter
public class Price {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Double initialPrice;

    private Integer discount;

    private Double priceWithDiscount;

    @OneToOne
    @JoinColumn(name = "product_id")
    private Product product;

    public Price(Double initialPrice) {
        this.initialPrice = initialPrice;
    }

    public Price(Double initialPrice, Integer discount) {
        this.initialPrice = initialPrice;
        this.discount = discount;
        this.priceWithDiscount = Math.floor((initialPrice*(100-discount))/100 * 100) / 100;
    }

    public static Price of(Double initialPrice, Integer discount){
        return new Price(initialPrice, discount);
    }

    public static Price of(Double initialPrice){
        return new Price(initialPrice);
    }

    public static Price fromDTO(PriceDTO priceDTO){
        if(priceDTO.getDiscount() == null){
            return of(priceDTO.getInitialPrice());
        }
        return of(priceDTO.getInitialPrice(), priceDTO.getDiscount());
    }

    public PriceDTO toDTO(){
        return PriceDTO.of(initialPrice, discount, priceWithDiscount);
    }

    public void setDiscount(Integer discount){
        this.discount = discount;
        this.priceWithDiscount = Math.floor((initialPrice*(100-discount))/100 * 100) / 100;
    }

    public void removeDiscount(){
        this.discount = null;
        this.priceWithDiscount = null;
    }

    @Override
    public String toString() {
        return "Price{" +
                "id=" + id +
                ", initialPrice=" + initialPrice +
                ", discount=" + discount +
                ", priceWithDiscount=" + priceWithDiscount +
                '}';
    }
}
