package com.plushkyIgrushky.model.product;

import com.plushkyIgrushky.dto.product.ProductDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;

    private String title;

    @OneToOne(mappedBy = "product", cascade = CascadeType.ALL)
    private Price price = new Price();

    private String description;

    private Integer quantity;

    @Enumerated(EnumType.STRING)
    @LazyCollection(LazyCollectionOption.FALSE)
    @ElementCollection(targetClass = LabelType.class)
    private List<LabelType> labels = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "mainImage_id", referencedColumnName = "id")
    private Image mainImage;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "product", cascade = CascadeType.ALL)
    private List<Image> images = new ArrayList<>();

    public Product(String title, Price price, String description, Integer quantity, List<LabelType> labels, Image mainImage, List<Image> images) {
        this.title = title;
        this.price = price;
        this.description = description;
        this.quantity = quantity;
        this.labels = labels;
        this.mainImage = mainImage;
        this.images = images;
    }

    public Product(Long id, String title, String description, Integer quantity, List<LabelType> labels) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.quantity = quantity;
        this.labels = labels;
    }

    public static Product of(Long id, String title, String description, Integer quantity, List<LabelType> labels){
        return new Product(id, title, description, quantity, labels);
    }

    public ProductDTO toDTO(){
        return ProductDTO.of(id, title, price.toDTO(), description, quantity, labels, mainImage.toDTO());
    }

    public static Product fromDTO(ProductDTO dto){
        return of(dto.getId(), dto.getTitle(), dto.getDescription(), dto.getQuantity(), dto.getLabels());
    }

    public void setMainImage(Image image){
        this.mainImage = image;
        image.setProduct(this);
        image.setMainProduct(this);
    }

    public void addImage(Image image){
        images.add(image);
        image.setProduct(this);
    }

    public void setPrice(Double initialPrice){
        this.price.setInitialPrice(initialPrice);
        this.price.setProduct(this);
    }

    public void setPrice(Price price){
        this.price = price;
        if(price.getDiscount() != null){
            setDiscount(price.getDiscount());
        }
        this.price.setProduct(this);
    }

    public void setDiscount(Integer discount){
        this.price.setDiscount(discount);
        if(!labels.contains(LabelType.SALE)){
            List<LabelType> newLabels = new ArrayList<>(labels);
            newLabels.add(LabelType.SALE);
            this.setLabels(newLabels);
        }
    }

    public void removeDiscount(){
        if(this.price.getDiscount() != null) {
            this.price.removeDiscount();
            List<LabelType> newLabels = new ArrayList<>(labels);
            newLabels.remove(LabelType.SALE);
            this.setLabels(newLabels);
        }
    }

}
