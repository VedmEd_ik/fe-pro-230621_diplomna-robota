package com.plushkyIgrushky.model.product;

import com.plushkyIgrushky.dto.product.ImageDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "images")
@NoArgsConstructor
@Getter
@Setter
public class Image {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String source;

    @OneToOne(mappedBy = "mainImage")
    private Product mainProduct;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    public Image(String source) {
        this.source = source;
    }

    public static Image of(String source){
        return new Image(source);
    }

    public ImageDTO toDTO(){
        return ImageDTO.of(source);
    }

    public static Image fromDTO(ImageDTO imageDTO){
        return of(imageDTO.getSource());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Image image = (Image) o;
        return Objects.equals(source, image.source);
    }

    @Override
    public int hashCode() {
        return Objects.hash(source);
    }

    @Override
    public String toString() {
        return "Image{" +
                "id=" + id +
                ", source='" + source +
                '}';
    }
}
