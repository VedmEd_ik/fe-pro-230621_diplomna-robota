package com.plushkyIgrushky.model.product.toy;

public enum ToyType {
    BEAR,
    UNICORN,
    RAT,
    RABBIT,
    DOG;
}
