package com.plushkyIgrushky.model.product.toy;

import com.plushkyIgrushky.dto.product.toy.ToyDTO;
import com.plushkyIgrushky.model.product.Image;
import com.plushkyIgrushky.model.product.LabelType;
import com.plushkyIgrushky.model.product.Price;
import com.plushkyIgrushky.model.product.Product;
import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "toys")
@NoArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
public class Toy extends Product {

    private Double height;

    @Enumerated(EnumType.STRING)
    private ToyType type;

    public Toy(String title, Price price, String description, Integer quantity, List<LabelType> labels, Image mainImage, List<Image> images, Double height, ToyType type) {
        super(title, price, description, quantity, labels, mainImage, images);
        this.height = height;
        this.type = type;
    }

    public Toy(Long id, String title, String description, Integer quantity, List<LabelType> labels, Double height, ToyType type) {
        super(id, title, description, quantity, labels);
        this.height = height;
        this.type = type;
    }

    public static Toy of(Long id, String title, String description, Integer quantity, List<LabelType> labels, Double height, ToyType type){
        return new Toy(id, title, description, quantity, labels, height, type);
    }

    public ToyDTO toDTO(){
        return ToyDTO.of(super.getId(), super.getTitle(), super.getPrice().toDTO(), super.getDescription(), super.getQuantity(), super.getLabels(), super.getMainImage().toDTO(), height, type);
    }

    public static Toy fromDTO(ToyDTO dto){
        return of(dto.getId(), dto.getTitle(),  dto.getDescription(), dto.getQuantity(), dto.getLabels(), dto.getHeight(), dto.getType());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Toy toy = (Toy) o;
        return Objects.equals(id, toy.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
