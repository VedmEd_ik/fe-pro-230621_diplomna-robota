package com.plushkyIgrushky.model.order;

import com.plushkyIgrushky.dto.order.CustomOrderDTO;
import com.plushkyIgrushky.model.user.CustomUser;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "CustomOrder")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CustomOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private CustomUser user;

    private Instant orderTimeStamp;

    private Double totalOrderPrice;

    public static CustomOrder of(Long id, CustomUser user, Instant orderTimeStamp, Double totalOrderPrice){
        return new CustomOrder(id, user, orderTimeStamp, totalOrderPrice);
    }

    public CustomOrderDTO toDTO(){
        return CustomOrderDTO.of(id, user.toDTO(), orderTimeStamp, totalOrderPrice);
    }

    public static CustomOrder fromDTO(CustomOrderDTO customOrderDTO){
        return of(customOrderDTO.getId(), CustomUser.fromDTO(customOrderDTO.getUser()), customOrderDTO.getOrderTimeStamp(), customOrderDTO.getTotalOrderPrice());
    }

    @Override
    public String toString() {
        return "CustomOrder{" +
                "id=" + id +
                ", user=" + user +
                ", orderTimeStamp=" + orderTimeStamp +
                ", totalOrderPrice=" + totalOrderPrice +
                '}';
    }
}
