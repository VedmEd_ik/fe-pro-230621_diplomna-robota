package com.plushkyIgrushky.model.order;

import com.plushkyIgrushky.dto.order.OrderDetailsDTO;
import com.plushkyIgrushky.model.product.Product;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "sss")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class OrderDetails implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private CustomOrder customOrder;

    @ManyToOne
    private Product product;

    private Integer quantity;

    public static OrderDetails of(Long id, CustomOrder customOrder, Product product, Integer quantity){
        return new OrderDetails(id, customOrder, product, quantity);
    }

    public OrderDetailsDTO toDTO(){
        return OrderDetailsDTO.of(id, customOrder.toDTO(), product.toDTO(), quantity);
    }

    public static OrderDetails fromDTO(OrderDetailsDTO orderDetailsDTO){
        return of(orderDetailsDTO.getId(), CustomOrder.fromDTO(orderDetailsDTO.getCustomOrder()), Product.fromDTO(orderDetailsDTO.getProduct()), orderDetailsDTO.getQuantity());
    }

    @Override
    public String toString() {
        return "OrderDetails{" +
                "id=" + id +
                ", customOrder=" + customOrder +
                ", product=" + product +
                ", quantity=" + quantity +
                '}';
    }
}
