package com.plushkyIgrushky.model.user;

import com.plushkyIgrushky.dto.user.AddressDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String country;

    private String city;

    private String street;

    private Integer houseNumber;

    @OneToOne(mappedBy = "address")
    private CustomUser user;

    public Address(String country, String city, String street, Integer houseNumber) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.houseNumber = houseNumber;
    }

    public static Address of(String country, String city, String street, Integer houseNumber){
        return new Address(country, city, street, houseNumber);
    }

    public AddressDTO toDTO(){
        return AddressDTO.of(country, city, street, houseNumber);
    }

    public static Address fromDTO(AddressDTO addressDTO){
        return of(addressDTO.getCountry(), addressDTO.getCity(), addressDTO.getStreet(), addressDTO.getHouseNumber());
    }
}
