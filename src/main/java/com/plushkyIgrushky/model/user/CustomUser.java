package com.plushkyIgrushky.model.user;

import com.plushkyIgrushky.dto.user.CustomUserDTO;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "CustomUser")
@NoArgsConstructor
@Getter
@Setter
public class CustomUser{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String firstName;
    private String username;
    private String email;
    private String password;

    @Enumerated(EnumType.STRING)
    private UserRole role;

    private String phone;

    @OneToOne(cascade = CascadeType.ALL)
    private Address address;

    public CustomUser(Long id, String firstName, String email, String password, UserRole role, String phone) {
        this.id = id;
        this.firstName = firstName;
        this.email = email;
        this.password = password;
        this.role = role;
        this.phone = phone;
    }

    public CustomUser(String firstName, String email, String password, UserRole role) {
        this.firstName = firstName;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public void addAddress(Address address){
        this.address = address;
        address.setUser(this);
    }

    public static CustomUser of(Long id, String firstName, String email, String password, UserRole role, String phone){
        return new CustomUser(id, firstName, email, password, role, phone);
    }

    public CustomUserDTO toDTO(){
        return CustomUserDTO.of(id, firstName, username, email, password, role, phone);
    }

    public static CustomUser fromDTO(CustomUserDTO customUserDTO){
        return of(customUserDTO.getId(), customUserDTO.getFirstName(), customUserDTO.getEmail(), customUserDTO.getPassword(), customUserDTO.getRole(), customUserDTO.getPhone());
    }

    @Override
    public String toString() {
        return "CustomUser{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                ", phone='" + phone + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CustomUser that = (CustomUser) o;
        return Objects.equals(username, that.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username);
    }
}
