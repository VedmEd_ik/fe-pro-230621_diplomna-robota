package com.plushkyIgrushky.utils;

import com.plushkyIgrushky.dto.product.ImageDTO;
import com.plushkyIgrushky.dto.product.PriceDTO;
import com.plushkyIgrushky.dto.product.toy.ToyDTO;
import com.plushkyIgrushky.model.product.Image;
import com.plushkyIgrushky.model.product.LabelType;
import com.plushkyIgrushky.model.product.Price;
import com.plushkyIgrushky.model.product.toy.Toy;
import com.plushkyIgrushky.model.product.toy.ToyType;

import java.util.ArrayList;
import java.util.List;

public class TestFactory {

    private String[] names = {"Мишка Тоша", "Єдиноріжка Луна", "Зайка Персик", "Ведмедик Пухляш", "Зайчик Лапа", "Мишка Боня", "Зайчик Побігайчик"};
    private String[] descriptions = {"Малеча", "Плюшик", "Дарує щастя", "Дарує любов"};
    private String[] sources = {"https://s9.gifyu.com/images/1621322897106-01_result.jpg", "https://s9.gifyu.com/images/1621683288759-01_result.jpg", "https://s9.gifyu.com/images/1621784222445-01_result.jpg", "https://s9.gifyu.com/images/1621934282479-01_result.jpg", "https://s9.gifyu.com/images/1622115380148-01_result.jpg", "https://s9.gifyu.com/images/1622279310310-01-3_result.jpg", "https://s9.gifyu.com/images/1622657716320-01-3_result.jpg", "https://s9.gifyu.com/images/1622906322903-01_result.jpg", "https://s9.gifyu.com/images/1623314726984-01_result.jpg", "https://s9.gifyu.com/images/1624537787038-01_result.jpg",
    "https://s9.gifyu.com/images/1625048144154-01_result.jpg", "https://s9.gifyu.com/images/1625646390564-01_result.jpg", "https://s9.gifyu.com/images/1626084621429-01_result.jpg", "https://s9.gifyu.com/images/1627294705056-01_result.jpg", "https://s9.gifyu.com/images/1627971031535-01_result.jpg", "https://s9.gifyu.com/images/1628180442916-01_result.jpg", "https://s9.gifyu.com/images/1628869565623-01_result.jpg", "https://s9.gifyu.com/images/1629366259403-01_result.jpg", "https://s9.gifyu.com/images/1629366451217-01_result.jpg", "https://s9.gifyu.com/images/1629995596542-01_result.jpg", "https://s9.gifyu.com/images/1630497400174-01_result.jpg",
    "https://s9.gifyu.com/images/1630765777912-01_result.jpg", "https://s9.gifyu.com/images/1631009141159-01_result.jpg", "https://s9.gifyu.com/images/1631441779682-01_result.jpg", "https://s9.gifyu.com/images/1631622933133-01_result.jpg", "https://s9.gifyu.com/images/1631771755473-01_result.jpg", "https://s9.gifyu.com/images/1631797481831-01_result.jpg", "https://s9.gifyu.com/images/1631798647847-01_result.jpg", "https://s9.gifyu.com/images/1632645194337-01_result.jpg", "https://s9.gifyu.com/images/1632733996156-01_result.jpg", "https://s9.gifyu.com/images/1632734295291-01_result.jpg", "https://s9.gifyu.com/images/1632734295291-01-01_result.jpg",
    "https://s9.gifyu.com/images/1632734409096-01_result.jpg", "https://s9.gifyu.com/images/1632755582698-01_result.jpg", "https://s9.gifyu.com/images/1632986889218-01_result.jpg", "https://s9.gifyu.com/images/1633067893528-01_result.jpg", "https://s9.gifyu.com/images/1633068350516-01_result.jpg", "https://s9.gifyu.com/images/1633421248362-01_result.jpg", "https://s9.gifyu.com/images/1633514421899-01_result.jpg", "https://s9.gifyu.com/images/1633679463674-01_result.jpg", "https://s9.gifyu.com/images/1633954253269-01_result.jpg", "https://s9.gifyu.com/images/1633966575010-01_result.jpg", "https://s9.gifyu.com/images/1633967007033-01_result.jpg", "https://s9.gifyu.com/images/1634971977967-01_result.jpg", "https://s9.gifyu.com/images/1636012811854-01_result.jpg", "https://s9.gifyu.com/images/1636356363974-01_result.jpg", "https://s9.gifyu.com/images/IMG_20210713_162457-01_result.jpg", "https://s9.gifyu.com/images/1621007763661-01-01-03_result.jpg"};

    public PriceDTO getPriceDTO(Toy toy){
        return toy.getPrice().toDTO();
    }

    public List<ImageDTO> getImages(Toy toy){
        List<ImageDTO> imageDTOS = new ArrayList<>();
        for(Image image : toy.getImages()){
            imageDTOS.add(image.toDTO());
        }
        return imageDTOS;
    }

    public ImageDTO getMainImage(Toy toy){
        return toy.getMainImage().toDTO();
    }

    public ToyDTO getToyDTO(Toy toy){
        return toy.toDTO();
    }

    public Toy createToy(){
        Price price = createPrice(false);
        if(getRandom(2) == 1){
            System.out.println(getRandom(2));
            price = createPrice(true);
        }
        int quantity = getRandom(20);
        double height = getRandom(20);
        int typeIndex = getRandom(ToyType.values().length);

        int sale = getRandom(2);
        if(sale == 1){
            return new Toy(createName(), price, createDescription(), quantity, List.of(LabelType.NEW), createMainImage(), createImages(), height, ToyType.values()[typeIndex]);
        }
        return new Toy(createName(), price, createDescription(), quantity, List.of(), createMainImage(), createImages(), height, ToyType.values()[typeIndex]);

    }

    private String createName(){
        return names[getRandom(names.length)];
    }

    private Price createPrice(boolean isDiscount){
        Double initialPrice = Math.floor(getRandom(1000.0) * 100) / 100;
        if(isDiscount){
            Integer discount = getRandom(80);
            return new Price(initialPrice, discount);
        }

        return new Price(initialPrice);
    }

    private String createDescription(){
        return descriptions[getRandom(descriptions.length)];
    }

    private Image createMainImage(){
        return new Image(sources[getRandom(sources.length)]);
    }

    private List<Image> createImages(){
        List<Image> images = new ArrayList<>();
        for(int i=0; i<6; i++){
            Image image = new Image(sources[getRandom(sources.length)]);
            images.add(image);
        }
        return images;
    }

    private int getRandom(int number){
        return (int) (Math.random() * number);
    }

    private double getRandom(double number){
        return Math.random() * number;
    }
}
