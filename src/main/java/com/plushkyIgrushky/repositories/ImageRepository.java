package com.plushkyIgrushky.repositories;

import com.plushkyIgrushky.model.product.Image;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ImageRepository extends JpaRepository<Image, Long> {
    List<Image> getImagesByProductId(Long id);
}
