package com.plushkyIgrushky.repositories;

import com.plushkyIgrushky.model.product.toy.Toy;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ToyRepository extends JpaRepository<Toy, Long> {
    Toy findByTitle(String title);
}
