package com.plushkyIgrushky.repositories;

import com.plushkyIgrushky.model.user.CustomUser;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<CustomUser, Long> {
    CustomUser findCustomUserByEmail(String email);
}
