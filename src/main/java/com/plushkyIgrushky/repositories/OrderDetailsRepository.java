package com.plushkyIgrushky.repositories;

import com.plushkyIgrushky.model.order.OrderDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderDetailsRepository extends JpaRepository<OrderDetails, Long> {
}
