package com.plushkyIgrushky.repositories;

import com.plushkyIgrushky.model.order.CustomOrder;
import com.plushkyIgrushky.model.user.CustomUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.Instant;

public interface OrderRepository extends JpaRepository<CustomOrder, Long> {
    CustomOrder findCustomOrderByOrderTimeStampAndUser(Instant orderDate, CustomUser user);
    CustomOrder findCustomOrderByUser(CustomUser user);
}
