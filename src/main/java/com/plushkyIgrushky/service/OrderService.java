package com.plushkyIgrushky.service;

import com.plushkyIgrushky.dto.order.CustomOrderDTO;

public interface OrderService {
    void addOrder(CustomOrderDTO customOrder);
}
