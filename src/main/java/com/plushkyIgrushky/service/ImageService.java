package com.plushkyIgrushky.service;

import com.plushkyIgrushky.dto.product.ImageDTO;

import java.util.List;

public interface ImageService{
    List<ImageDTO> findImagesByProduct(Long id);
}
