package com.plushkyIgrushky.service;

import com.plushkyIgrushky.dto.results.ResultDTO;
import com.plushkyIgrushky.dto.user.CustomUserDTO;
import org.springframework.http.ResponseEntity;

public interface UserService {
    boolean addUser(CustomUserDTO userDTO);
    ResponseEntity<ResultDTO> login(String email, String password);
}
