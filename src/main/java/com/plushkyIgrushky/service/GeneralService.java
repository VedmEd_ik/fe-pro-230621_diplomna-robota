package com.plushkyIgrushky.service;

import com.plushkyIgrushky.dto.product.ImageDTO;
import com.plushkyIgrushky.dto.product.PriceDTO;
import com.plushkyIgrushky.dto.product.toy.ToyDTO;
import com.plushkyIgrushky.dto.order.CustomOrderDTO;
import com.plushkyIgrushky.dto.order.OrderDetailsDTO;
import com.plushkyIgrushky.dto.results.*;
import com.plushkyIgrushky.dto.user.CustomUserDTO;
import com.plushkyIgrushky.model.order.CustomOrder;
import com.plushkyIgrushky.model.order.OrderDetails;
import com.plushkyIgrushky.model.product.Image;
import com.plushkyIgrushky.model.product.Price;
import com.plushkyIgrushky.model.product.Product;
import com.plushkyIgrushky.model.product.toy.Toy;
import com.plushkyIgrushky.model.user.CustomUser;
import com.plushkyIgrushky.model.user.UserRole;
import com.plushkyIgrushky.repositories.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class GeneralService implements ToyService, ImageService, UserService, OrderService, OrderDetailsService{

    private final ToyRepository toyRepository;
    private final ImageRepository imageRepository;
    private final OrderRepository orderRepository;
    private final UserRepository userRepository;
    private final OrderDetailsRepository orderDetailsRepository;
    private final PasswordEncoder passwordEncoder;

    public GeneralService(ToyRepository toyRepository, ImageRepository imageRepository, OrderRepository orderRepository, UserRepository userRepository, OrderDetailsRepository orderDetailsRepository, PasswordEncoder passwordEncoder) {
        this.toyRepository = toyRepository;
        this.imageRepository = imageRepository;
        this.orderRepository = orderRepository;
        this.userRepository = userRepository;
        this.orderDetailsRepository = orderDetailsRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void addOrderDetails(OrderDetailsDTO orderDetailsDTO) {
        OrderDetails orderDetails = OrderDetails.fromDTO(orderDetailsDTO);

        CustomUser orderUser = userRepository.findCustomUserByEmail(orderDetails.getCustomOrder().getUser().getEmail());

        Instant orderDate = orderDetails.getCustomOrder().getOrderTimeStamp();
        if (orderDate.getNano() >= 500000000L){
            orderDate = orderDate.truncatedTo(ChronoUnit.SECONDS).plusSeconds(1);
        } else{
            orderDate = orderDate.truncatedTo(ChronoUnit.SECONDS);
        }

        CustomOrder order = orderRepository.findCustomOrderByOrderTimeStampAndUser(orderDate, orderUser);

        Product orderProduct = toyRepository.findByTitle(orderDetails.getProduct().getTitle());

        orderDetails.setProduct(orderProduct);

        orderDetails.setCustomOrder(order);

        orderDetailsRepository.save(orderDetails);
    }

    @Transactional
    public boolean addUser(CustomUserDTO newUser) {
        CustomUser user = userRepository.findCustomUserByEmail(newUser.getEmail());

        if(user != null){
            return false;
        }else{
            user = CustomUser.fromDTO(newUser);
        }

        userRepository.save(user);

        return true;
    }

    @Transactional
    public ResponseEntity<ResultDTO> login(String email, String password){
        CustomUser user = userRepository.findCustomUserByEmail(email);

        if(user == null){
            return new ResponseEntity<>(new UserNotExists(), HttpStatus.OK);
        }

        if(!passwordEncoder.matches(password, user.getPassword())){
            return new ResponseEntity<>(new WrongPassword(), HttpStatus.OK);
        }

        if(user.getRole().equals(UserRole.ADMIN)){
            return new ResponseEntity<>(new AdminLogin(), HttpStatus.OK);
        }

        return new ResponseEntity<>(new SuccessResult(), HttpStatus.OK);
    }

    @Transactional
    public void addOrder(CustomOrderDTO customOrderDTO) {
        CustomOrder order = CustomOrder.fromDTO(customOrderDTO);

        CustomUser user = userRepository.findCustomUserByEmail(customOrderDTO.getUser().getEmail());

        order.setUser(user);

        orderRepository.save(order);
    }

    @Transactional(readOnly = true)
    public List<ToyDTO> getToys(Pageable pageable){
        Page<Toy> toys = toyRepository.findAll(pageable);
        List<ToyDTO> toyDTOS = new ArrayList<>();

        for(Toy toy : toys){
            toyDTOS.add(toy.toDTO());
        }

        return toyDTOS;
    }

    @Override
    @Transactional
    public void addToy(ToyDTO newToy, PriceDTO price, ImageDTO mainImage, List<ImageDTO> images){
        Toy toy = Toy.fromDTO(newToy);

        images.forEach((x) -> {
            Image image = Image.fromDTO(x);
            toy.addImage(image);
        });

        toy.setMainImage(Image.fromDTO(mainImage));

        toy.setPrice(Price.fromDTO(price));

        toyRepository.save(toy);
    }

    @Transactional(readOnly = true)
    public List<ToyDTO> getNew(){
        List<ToyDTO> newToys = new ArrayList<>();

        return newToys;
    }

    @Transactional
    public void setToysOnSale(List<Long> ids, Integer discount){
        ids.forEach(id -> {
            Optional<Toy> toy = toyRepository.findById(id);

            toy.ifPresent(t -> {
                t.setDiscount(discount);
            });

        });
    }

    @Override
    @Transactional(readOnly = true)
    public List<ImageDTO> findImagesByProduct(Long id) {
        List<ImageDTO> result = new ArrayList<>();
        List<Image> images = imageRepository.getImagesByProductId(id);

        images.forEach((x) -> {
            result.add(x.toDTO());
        });

        return result;
    }
}
