package com.plushkyIgrushky.service;

import com.plushkyIgrushky.dto.product.ImageDTO;
import com.plushkyIgrushky.dto.product.PriceDTO;
import com.plushkyIgrushky.dto.product.toy.ToyDTO;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ToyService {

    List<ToyDTO> getToys(Pageable pageable);

    void addToy(ToyDTO toy, PriceDTO price, ImageDTO mainImage, List<ImageDTO> images);

    List<ToyDTO> getNew();

    void setToysOnSale(List<Long> ids, Integer discount);

}
