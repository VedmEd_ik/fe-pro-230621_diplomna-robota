package com.plushkyIgrushky.service;

import com.plushkyIgrushky.dto.order.OrderDetailsDTO;

public interface OrderDetailsService {
    void addOrderDetails(OrderDetailsDTO orderDetails);
}
