package com.plushkyIgrushky.config;

import com.plushkyIgrushky.dto.product.ImageDTO;
import com.plushkyIgrushky.dto.product.PriceDTO;
import com.plushkyIgrushky.dto.product.toy.ToyDTO;
import com.plushkyIgrushky.dto.order.CustomOrderDTO;
import com.plushkyIgrushky.dto.user.CustomUserDTO;
import com.plushkyIgrushky.model.order.CustomOrder;
import com.plushkyIgrushky.model.order.OrderDetails;
import com.plushkyIgrushky.model.product.toy.Toy;
import com.plushkyIgrushky.model.user.CustomUser;
import com.plushkyIgrushky.model.user.UserRole;
import com.plushkyIgrushky.service.UserService;
import com.plushkyIgrushky.service.OrderDetailsService;
import com.plushkyIgrushky.service.OrderService;
import com.plushkyIgrushky.service.ToyService;
import com.plushkyIgrushky.utils.TestFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

@Configuration
@PropertySource("classpath:application.properties")
public class AppConfig {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public CommandLineRunner demo(final ToyService toyService, final OrderService orderService, final UserService userService, final OrderDetailsService orderDetailsService) {
        return new CommandLineRunner() {
            @Override
            public void run(String... args) throws Exception {

                /*************************************************************\
                *                                                             *
                *                          ADMIN USER                         *
                *                                                             *
                \*************************************************************/

                CustomUser customUser = new CustomUser();
                customUser.setEmail("admin");
                customUser.setPassword(passwordEncoder().encode("admin"));
                customUser.setRole(UserRole.ADMIN);

                CustomUserDTO userDTO = customUser.toDTO();

                userService.addUser(userDTO);

                /*************************************************************\
                *                                                             *
                *                          TEST TOYS                          *
                *                                                             *
                \*************************************************************/

                TestFactory testFactory = new TestFactory();
                for(int i=0; i<21; i++){
                    Toy toy = testFactory.createToy();
                    ToyDTO toyDTO = testFactory.getToyDTO(toy);
                    PriceDTO priceDTO = testFactory.getPriceDTO(toy);
                    ImageDTO mainImageDTO = testFactory.getMainImage(toy);
                    List<ImageDTO> imageDTOS = testFactory.getImages(toy);
                    toyService.addToy(toyDTO, priceDTO, mainImageDTO, imageDTOS);
                }

                // CAN BE TESTED ONLY IN REAL CASE WITH UNIQUE NAMES

//                /*************************************************************\
//                *                                                             *
//                *                          TEST ORDER                         *
//                *                                                             *
//                \*************************************************************/
//
//                CustomOrder customOrder = new CustomOrder();
//                customOrder.setUser(customUser);
//                customOrder.setOrderTimeStamp(Instant.now());
//                customOrder.setTotalOrderPrice(322.0);
//
//                CustomOrderDTO orderDTO = customOrder.toDTO();
//
//                orderService.addOrder(orderDTO);
//
//                /*************************************************************\
//                *                                                             *
//                *                      TEST ORDER DETAILS                     *
//                *                                                             *
//                \*************************************************************/
//
//                OrderDetails orderDetailsTest = new OrderDetails();
//
//                orderDetailsTest.setCustomOrder(customOrder);
//                orderDetailsTest.setQuantity(3);
//                orderDetailsTest.setProduct(toy);
//
//                orderDetailsService.addOrderDetails(orderDetailsTest.toDTO());
//
//                OrderDetails orderDetailsTest2 = new OrderDetails();
//
//                orderDetailsTest2.setCustomOrder(customOrder);
//                orderDetailsTest2.setQuantity(3);
//                orderDetailsTest2.setProduct(toy);
//
//                orderDetailsService.addOrderDetails(orderDetailsTest2.toDTO());
            }
        };
    }
}