package com.plushkyIgrushky.dto.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
public class AddressDTO {

    private String country;

    private String city;

    private String street;

    private Integer houseNumber;

    public AddressDTO(String country, String city, String street, Integer houseNumber) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.houseNumber = houseNumber;
    }

    public static AddressDTO of(String country, String city, String street, Integer houseNumber){
        return new AddressDTO(country, city, street, houseNumber);
    }
}
