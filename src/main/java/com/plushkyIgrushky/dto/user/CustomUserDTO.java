package com.plushkyIgrushky.dto.user;

import com.plushkyIgrushky.model.user.UserRole;
import lombok.Data;

@Data
public class CustomUserDTO {

    private Long id;

    private String firstName;
    private String username;
    private String email;
    private String password;

    private UserRole role;

    private String phone;
    private AddressDTO address;

    public CustomUserDTO(Long id, String firstName, String username, String email, String password, UserRole role, String phone) {
        this.id = id;
        this.firstName = firstName;
        this.username = username;
        this.email = email;
        this.password = password;
        this.role = role;
        this.phone = phone;
    }

    public static CustomUserDTO of(Long id, String firstName, String username, String email, String password, UserRole role, String phone){
        return new CustomUserDTO(id, firstName, username, email, password, role, phone);
    }
}
