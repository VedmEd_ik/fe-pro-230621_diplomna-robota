package com.plushkyIgrushky.dto.order;

import com.plushkyIgrushky.dto.user.CustomUserDTO;
import lombok.Data;

import java.time.Instant;

@Data
public class CustomOrderDTO {

    private Long id;

    private CustomUserDTO user;

    private Instant orderTimeStamp;

    private Double totalOrderPrice;

    public CustomOrderDTO(Long id, CustomUserDTO user, Instant orderTimeStamp, Double totalOrderPrice) {
        this.id = id;
        this.user = user;
        this.orderTimeStamp = orderTimeStamp;
        this.totalOrderPrice = totalOrderPrice;
    }

    public static CustomOrderDTO of(Long id, CustomUserDTO user, Instant orderTimeStamp, Double totalOrderPrice){
        return new CustomOrderDTO(id, user, orderTimeStamp, totalOrderPrice);
    }
}
