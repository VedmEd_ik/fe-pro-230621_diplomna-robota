package com.plushkyIgrushky.dto.order;

import com.plushkyIgrushky.dto.product.ProductDTO;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class OrderDetailsDTO {

    private Long id;

    private CustomOrderDTO customOrder;

    private ProductDTO product;

    private Integer quantity;

    public static OrderDetailsDTO of(Long id, CustomOrderDTO order, ProductDTO product, Integer quantity){
        return new OrderDetailsDTO(id, order, product, quantity);
    }
}
