package com.plushkyIgrushky.dto.product;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ImageDTO {

    private String source;

    public ImageDTO(String source) {
        this.source = source;
    }

    public static ImageDTO of(String source){
        return new ImageDTO(source);
    }
}
