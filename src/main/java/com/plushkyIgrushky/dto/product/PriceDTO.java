package com.plushkyIgrushky.dto.product;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PriceDTO {

    private Double initialPrice;

    private Integer discount;

    private Double priceWithDiscount;

    public PriceDTO(Double initialPrice, Integer discount, Double priceWithDiscount) {
        this.initialPrice = initialPrice;
        this.discount = discount;
        this.priceWithDiscount = priceWithDiscount;
    }

    public static PriceDTO of(Double initialPrice, Integer discount, Double priceWithDiscount){
        return new PriceDTO(initialPrice, discount, priceWithDiscount);
    }

    @Override
    public String toString() {
        return "PriceDTO{" +
                "initialPrice=" + initialPrice +
                ", discount=" + discount +
                ", priceWithDiscount=" + priceWithDiscount +
                '}';
    }
}
