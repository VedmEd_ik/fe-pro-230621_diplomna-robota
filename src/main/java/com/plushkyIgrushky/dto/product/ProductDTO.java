package com.plushkyIgrushky.dto.product;

import com.plushkyIgrushky.model.product.LabelType;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class ProductDTO {

    private Long id;

    private String title;

    private PriceDTO price;

    private String description;

    private Integer quantity;

    private List<LabelType> labels = new ArrayList<>();

    private ImageDTO mainImage;

    public ProductDTO(Long id, String title, PriceDTO price, String description, Integer quantity, List<LabelType> labels, ImageDTO mainImage) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.description = description;
        this.quantity = quantity;
        this.labels = labels;
        this.mainImage = mainImage;
    }

    public ProductDTO(String title, String description, Integer quantity, List<LabelType> labels, ImageDTO mainImage) {
        this.title = title;
        this.description = description;
        this.quantity = quantity;
        this.labels = labels;
        this.mainImage = mainImage;
    }

    public static ProductDTO of(Long id, String title, PriceDTO price, String description, Integer quantity, List<LabelType> labels, ImageDTO mainImage){
        return new ProductDTO(id, title, price, description, quantity, labels, mainImage);
    }
}
