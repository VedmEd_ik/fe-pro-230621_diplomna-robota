package com.plushkyIgrushky.dto.product;

import com.plushkyIgrushky.dto.product.toy.ToyDTO;
import com.plushkyIgrushky.model.product.LabelType;
import com.plushkyIgrushky.model.product.toy.ToyType;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Setter
public class PublishedToyDTO {

    private String title;
    private PriceDTO price;
    private String description;
    private Integer quantity;
    private List<LabelType> labels;
    private ImageDTO mainImage;
    private Double height;
    private ToyType type;
    private List<ImageDTO> images;

    public ToyDTO getToyDTO(){
        return new ToyDTO(title, description, quantity, labels, mainImage, height, type);
    }

    public PriceDTO getPriceDTO(){
        return price;
    }

    public List<ImageDTO> getImageDTO(){
        return images;
    }

    public ImageDTO getMainImage(){
        return mainImage;
    }

    @Override
    public String toString() {
        return "NewToyDTO{" +
                "title='" + title + '\'' +
                ", priceDTO=" + price +
                ", description='" + description + '\'' +
                ", quantity=" + quantity +
                ", labels=" + labels +
                ", mainImage=" + mainImage +
                ", height=" + height +
                ", type=" + type +
                ", images=" + images +
                '}';
    }
}
