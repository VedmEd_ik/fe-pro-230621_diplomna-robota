package com.plushkyIgrushky.dto.product.toy;

import com.plushkyIgrushky.dto.product.ImageDTO;
import com.plushkyIgrushky.dto.product.PriceDTO;
import com.plushkyIgrushky.dto.product.ProductDTO;
import com.plushkyIgrushky.model.product.LabelType;
import com.plushkyIgrushky.model.product.toy.ToyType;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;

@Data
@ToString(callSuper = true)
@NoArgsConstructor
public class ToyDTO extends ProductDTO {

    private Double height;

    private ToyType type;

    public ToyDTO(Long id, String title, PriceDTO price, String description, Integer quantity, List<LabelType> labels, ImageDTO mainImage, Double height, ToyType type) {
        super(id, title, price, description, quantity, labels, mainImage);
        this.height = height;
        this.type = type;
    }

    public ToyDTO(String title, String description, Integer quantity, List<LabelType> labels, ImageDTO mainImage, Double height, ToyType type) {
        super(title, description, quantity, labels, mainImage);
        this.height = height;
        this.type = type;
    }

    public static ToyDTO of(Long id, String title, PriceDTO price, String description, Integer quantity, List<LabelType> labels, ImageDTO mainImage, Double height, ToyType type){
        return new ToyDTO(id, title, price, description, quantity, labels, mainImage, height, type);
    }

}
