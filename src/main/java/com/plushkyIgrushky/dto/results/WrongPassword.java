package com.plushkyIgrushky.dto.results;

public class WrongPassword extends ResultDTO{
    public WrongPassword() {
        super("Wrong password");
    }
}
