package com.plushkyIgrushky.dto.results;

public class UserExists extends ResultDTO {
    public UserExists(){
        super("User already exists");
    }
}
