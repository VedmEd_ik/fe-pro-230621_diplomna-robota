package com.plushkyIgrushky.dto.results;

public class UserNotExists extends ResultDTO{
    public UserNotExists() {
        super("User does not exist");
    }
}
