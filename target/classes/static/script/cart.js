'use strict'

// Додавання польоту товарів в кошик
function addToCartFly(productId, productCardButton) {
	if (!productCardButton.classList.contains('hold')) {
		productCardButton.classList.add('hold');
		productCardButton.classList.add('fly');
	};
	const cart = document.querySelector('.cart__icon-link');
	const product = document.querySelector(`[data-prodid='${productId}']`);
	const productImage = product.querySelector('.product-card__img img');

	// Клоную картку продукту
	const productImageFly = productImage.cloneNode(true);

	const productImageFlyWidth = productImage.offsetWidth;
	const productImageFlyHeight = productImage.offsetHeight;
	const productImageFlyTop = productImage.getBoundingClientRect().top;
	const productImageFlyLeft = productImage.getBoundingClientRect().left;

	productImageFly.setAttribute('class', 'flyImage');
	productImageFly.style.cssText =
		`
		left:${productImageFlyLeft}px;
		top:${productImageFlyTop}px;
		widt:${productImageFlyWidth}px;
		height:${productImageFlyHeight}px;
		`;

	document.body.append(productImageFly);

	const cartFlyLeft = cart.getBoundingClientRect().left;
	const cartFlyTop = cart.getBoundingClientRect().top;

	productImageFly.style.cssText =
		`
		left: ${cartFlyLeft}px;
		top: ${cartFlyTop}px;
		widt: 0px;
		height: 0px;
		opacity: 0;
		`;

	productImageFly.addEventListener('transitionend', function () {
		if (productCardButton.classList.contains('fly')) {
			productImageFly.remove();
			productCardButton.classList.remove('fly');
		}
	});
	updateCart();
}

// Функція перевірки чи є вже даний товар в кошику. Якщо є, то збільшити його кількість, якщо нема - добавити
function productInCart(productId, productCardButton) {
	// Отримуємо корзину
	const cart = document.querySelector('.cart__products-in-cart');
	// Отримуємо в масив усі ід товарів, які є в корзині
	const allProductsId = [].map.call(cart.querySelectorAll('.cart__product-item[data-prodid]'), function (el) {
		return el.dataset.prodid;
	});
	// Перевіряємо чи товар, який додається уже наявний в кошику
	if (allProductsId.includes(productId)) {
		// Якщо є, то просто добавляємо анімацію польоту товару в кошик і збільшуємо його кількість на 1
		addToCartFly(productId, productCardButton);
		const product = cart.querySelector(`[data-prodid='${productId}']`);
		product.children[1].children[0].value = parseInt(product.children[1].children[0].value) + 1;
	} else {
		// Якщо товару нема, то добавляємо анімацію польоту і додаємо товар в кошик
		addToCartFly(productId, productCardButton);
		addToCart(productId);
	}
	totalSumItem();
};

// Функція додавання товару в кошик
function addToCart(productId) {
	// Отримую вихідні дані
	const product = document.querySelector(`[data-prodid='${productId}']`);
	const productImage = product.querySelector('.product-card__img img');
	const productTitle = product.querySelector('.product-card__title').textContent;

	// Отримання актуальної ціни товару
	let productPrice = null;
	// Ціна із знижкою
	if (product.querySelector('.product-card__price-widh-discount')) {
		const priceWidhDiscount = parseInt(product.querySelector('.product-card__price-widh-discount').innerHTML.match(/\d+/));
		productPrice = priceWidhDiscount;
	} else {
		// Ціна без знижки
		const priceWithoutDiscount = parseInt(product.querySelector('.product-card__price-without-discount').innerHTML.match(/\d+/));
		productPrice = priceWithoutDiscount;
	}

	const cartTotal = document.querySelector('.cart__total');

	const productItem = document.createElement('div');
	productItem.classList.add('cart__product-item');
	productItem.dataset.prodid = productId;
	cartTotal.before(productItem);

	const nameProduct = document.createElement('div');
	nameProduct.classList.add('cart__name-product');
	productItem.append(nameProduct);

	const wrapImgProduct = document.createElement('div');
	wrapImgProduct.classList.add('cart__img-product');
	nameProduct.append(wrapImgProduct);

	const imgProduct = document.createElement('img');
	imgProduct.setAttribute('src', productImage.src);
	imgProduct.setAttribute('alt', 'image');
	wrapImgProduct.append(imgProduct);

	const urlProduct = document.createElement('a');
	urlProduct.setAttribute('href', '#');
	urlProduct.textContent = productTitle;
	wrapImgProduct.append(urlProduct);

	const countProduct = document.createElement('div');
	countProduct.classList.add('cart__count-product');
	productItem.append(countProduct);

	const inputCountProduct = document.createElement('input');
	inputCountProduct.setAttribute('type', 'number');
	inputCountProduct.setAttribute('min', '1');
	inputCountProduct.setAttribute('max', '100');
	inputCountProduct.setAttribute('value', '1');
	inputCountProduct.setAttribute('size', '3');
	countProduct.append(inputCountProduct);

	const priceProduct = document.createElement('div');
	priceProduct.classList.add('cart__price-product');
	priceProduct.textContent = productPrice;
	productItem.append(priceProduct);

	const sumProduct = document.createElement('div');
	sumProduct.classList.add('cart__sum-product');
	productItem.append(sumProduct);


	const deleteProduct = document.createElement('div');
	deleteProduct.classList.add('cart__delete-product');
	productItem.append(deleteProduct);

	const linkImgDeleteProduct = document.createElement('a');
	linkImgDeleteProduct.setAttribute('href', '#');
	linkImgDeleteProduct.classList.add('delete-product-button');
	deleteProduct.append(linkImgDeleteProduct);

	const imgDeleteProduct = document.createElement('img');
	imgDeleteProduct.setAttribute('src', './image/icon/trashcan_delete.svg');
	imgDeleteProduct.classList.add('delete-product-button');
	linkImgDeleteProduct.append(imgDeleteProduct);

	totalSumItem();

	// Підраховуємо загальну вартість однієї позиції (іграшок одного типу) (у випадку якщо покупець в корзині збільшить/зменшить кількість іграшок)
	inputCountProduct.addEventListener('change', totalSumItem);
}

// Функція для підрахунку загальної вартості однієї позиції (іграшок одного типу)
function totalSumItem() {
	const cart = document.querySelector('.cart-wrapper');
	const allProductInCart = cart.querySelectorAll('.cart__product-item');
	if (allProductInCart.length > 0) {
		allProductInCart.forEach((product) => {
			const pCount = parseInt(product.children[1].children[0].value);
			const pPrice = parseInt(product.children[2].textContent);
			product.children[3].textContent = pCount * pPrice;
		});
	}
	totalSumCart();
	updateCart();
}

// Функція підрахунку загальної вартості корзини
function totalSumCart() {
	const allPriceProductsInCart = document.querySelectorAll('.cart__sum-product');
	const resultSum = document.querySelector('.cart__total-sum');
	let totalSum = null;
	if (allPriceProductsInCart.length > 0) {
		allPriceProductsInCart.forEach(productPrice => {
			totalSum += +productPrice.textContent;
		});
		resultSum.textContent = `на суму ${totalSum} грн.`;
	} else {
		resultSum.textContent = '';
	};
};

// Функція підрахунку загальної кількості товарів в корзині
function totalCountProductsInCart() {
	const cartTotalInfo = document.querySelector('.cart__total-info');
	const cartTotalCountProducts = document.querySelector(".cart__total-count-products");
	const allProductsInCart = document.querySelectorAll('.cart__count-product [type="number"]');
	let countProductInCart = 0;
	if (allProductsInCart.length > 0) {
		allProductsInCart.forEach(count => {
			countProductInCart += parseInt(count.value);
		});
		cartTotalInfo.textContent = "Підсумок: ";
		cartTotalCountProducts.textContent = `іграшок: ${countProductInCart} `;
	} else {
		cartTotalInfo.textContent = "";
		cartTotalCountProducts.textContent = "Ви не обрали жодної іграшки";
	}
	return countProductInCart;
};

// Функція оновлення лічильника доданих товарів в кошик біля іконки кошика
function updateCart() {

	const cart = document.querySelector('.header__cart');
	const cartIcon = cart.querySelector('.cart__icon-link');
	const cartSpan = cartIcon.querySelector('span');
	const cartQuantity = totalCountProductsInCart();

	if (cartQuantity === 0) {
		if (cartSpan) {
			cartSpan.remove();
		}
	} else {
		if (cartSpan) {
			cartSpan.innerHTML = cartQuantity;
		} else {
			cartIcon.insertAdjacentHTML('beforeend', `<span>${cartQuantity}</span>`)
		};
	}
}

// Функція формування та надіслання замовлення
function sendOrder() {
	const order = [];
	// Отримання данних про товари з кошика
	const cart = document.querySelector('.cart-wrapper');
	const allProductInCart = cart.querySelectorAll('.cart__product-item');
	if (allProductInCart.length > 0) {
		allProductInCart.forEach((product, index) => {
			order[index] = {};
			order[index].pId = product.getAttribute('data-prodid');
			order[index].pName = product.children[0].textContent;
			order[index].pImg = product.children[0].children[0].children[0].src;
			order[index].pCount = product.children[1].children[0].value;
			order[index].pPrice = product.children[2].textContent;
			order[index].pSum = product.children[3].textContent;
		});
	} else {
		alert("Ви не обрали жодної іграшки!");
		return;
	};

	const payAndDelivery = {
		pay: null,
		delivery: null,
		adressDelivery: null,
		tel: null
	};

	// Отримання данних форми
	const cartForm = cart.querySelector('.cart-form');
	const cash = cartForm.querySelector('#cash').checked;
	const cashless = cartForm.querySelector('#cashless').checked;
	const ukrposhta = cartForm.querySelector('#ukrposhta').checked;
	const novaposhta = cartForm.querySelector('#novaposhta').checked;
	const adressDelivery = cartForm.querySelector('#delivery-adress').value;
	const tel = cartForm.querySelector('#tel').value;
	if (cash) {
		payAndDelivery.pay = 'Оплата при отримані (наложеним платежем)';
	};
	if (cashless) {
		payAndDelivery.pay = 'Передоплата - безготівковий переказ';
	};
	if (ukrposhta) {
		payAndDelivery.delivery = 'Спосіб доставки - Укрпошта';
	};
	if (novaposhta) {
		payAndDelivery.delivery = 'Спосіб доставки - Нова пошта';
	};
	if (adressDelivery) {
		payAndDelivery.adressDelivery = `Адреса для доставки: ${adressDelivery}`;
		cartForm.querySelector('#delivery-adress').style.border = "none";
	} else {
		cartForm.querySelector('#delivery-adress').style.border = "solid 1px red";
		return;
	};

	// Валідація номера телефону
	const regex = /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/;
	const phone = tel;
	// console.dir(cartForm.querySelector('#tel').value);
	if (!regex.test(phone)) {
		cartForm.querySelector('#tel').style.border = "solid 1px red";
		return;
	} else {
		payAndDelivery.tel = `Телефон: ${phone}`;
	};

	order.push(payAndDelivery);
	sendOrderToServer({ order });
	console.dir(JSON.stringify(order));
};

// Функція "Дякую за замовлення"
function thankForOrder() {
	const allProductInCart = document.querySelectorAll('.cart__product-item');
	const cartForm = document.querySelector('.cart-form');

	allProductInCart.forEach(elem => {
		elem.remove();
	});

	cartForm.reset();
	updateCart();
	totalSumCart();
	document.querySelector('.cart__icon-link span').remove();
	document.querySelector('.cart-wrapper').style.display = 'none';

	const thankForOrder = document.createElement('div');
	thankForOrder.classList.add('thank-for-order');
	thankForOrder.innerHTML = "Дякую!<br> Ваше замовлення прийнято.<br> Я обовязково звяжуся з вами найближчим часом.";
	document.querySelector('.wrapper').append(thankForOrder);
	setTimeout(() => thankForOrder.remove(), 4000);
}