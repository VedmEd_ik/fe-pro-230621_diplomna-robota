document.addEventListener('DOMContentLoaded', () => {

	// Делегування події "клік"
	document.addEventListener('click', (event) => {
		const targetElement = event.target;

		// Клік по кнопці "Вихід"
		if (targetElement.closest('.exit')) {
			window.open('../index.html', "_self");
		};

		// Клік по посиланню "Мої замовлення"
		if (targetElement.closest('.menu__my-order')) {
			document.querySelector('.content__change-my-profile').style.display = 'none';
			document.querySelector('.content__my-order').style.display = 'flex';
			document.querySelector('.wrapper h1').textContent = targetElement.textContent;
		};

		// Клік по посиланню "Редагувати мій профіль"
		if (targetElement.closest('.menu__change-my-profile')) {
			document.querySelector('.content__change-my-profile').style.display = 'flex';
			document.querySelector('.content__my-order').style.display = 'none';
			document.querySelector('.wrapper h1').textContent = targetElement.textContent;
		};
	});
});